﻿using System;

namespace Task8a
{
    class Program
    {
        static void Main(string[] args)
        {
            double BMI = CalculateBMI();
            string status = GetStatus(BMI);
            Console.WriteLine("Your BMI is {0} and you are {1}", BMI, status);
            }
        public static double CalculateBMI()
        {
            Console.Write("What is your Weight (Kg)");
            double Weight = Convert.ToDouble(Console.ReadLine());
            Console.Write("What is your Height (m)");
            double Height = Convert.ToDouble(Console.ReadLine());
            double BMI = Weight / (Height * Height);
            return (BMI);
        }

        public static string GetStatus(double BMI)
        {
            string status = "Unknown";
            if (BMI < 18.5)
            {
                status = "Underweight";
            }
            if (BMI < 24.9 && BMI >= 18.5)
            {
                status = "Normal weight";
            }
            if (BMI < 29.9 && BMI >= 24.9)
            {
                status = "Overweight";
            }
            if (BMI >= 30)
            {
                status = "Obese";
            }
            return status;
        }
    }
}
