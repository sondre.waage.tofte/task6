﻿using System;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("What is your Weight (Kg)");
            double Weight = Convert.ToDouble(Console.ReadLine());
            Console.Write("What is your Height (m)");
            double Height = Convert.ToDouble(Console.ReadLine());
            double BMI = Weight / (Height*Height);
            string status = "Unknown";
            if (BMI < 18.5)
            {
                status = "Underweight";
            }
            if (BMI < 24.9 && BMI >= 18.5)
            {
                status = "Normal weight";
            }
            if (BMI < 29.9 && BMI >= 24.9)
            {
                status = "Overweight";
            }
            if (BMI >= 30)
            {
                status = "Obese";
            }
            Console.WriteLine("Your BMI is {0} and you are {1}", BMI, status);
        }
    }
}
